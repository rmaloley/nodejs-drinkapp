# NODEJS-DRINKAPP #

This is a simple webapp written in Node.JS, Express, and EJS. It connects to a mongodb instance and allows you to submit your favorite drink. You can also delete the entire DB in a single click! 

I wrote this in order to continue to demonstrate the power of automation and orchestration with Ansible. 

All code is open to anyone.

Initial code and design is based on: https://github.com/zellwk/crud-express-mongo

# Instructions #

1. Clone this repo to your location.
2. Modify the MongoDB connection information in index.js. 
3. Install Node JS.
4. Change your working directory to where you extracted the application.
5. npm install
6. npm install pm2
7. Start the application with "node index.js" or "pm2 start index.jsp"

# Deploy Script #
I have also included a simple deployment script that does all the above. Simply download deploy-drinkapp.sh locally, modify to your requirements, and execute. 

You can also use this as a re-deploy script when code changes in the repository.

# Support #
I promise zero support - however you can always ask. :)
