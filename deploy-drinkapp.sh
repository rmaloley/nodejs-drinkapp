#!/bin/bash

# Set some variables
DRINKAPPHOME="/var/drinkapp"

# Update our PATH
PATH=$PATH:$DRINKAPPHOME/node_modules/pm2/bin

# If our directory currently exists then delete it so we have clean files.
if [ -d "$DRINKAPPHOME" ]; then
    rm -rf $DRINKAPPHOME
fi

# Get our webapp files from bitbucket
git clone https://rmaloley@bitbucket.org/rmaloley/nodejs-drinkapp.git $DRINKAPPHOME

# Change our working directory
cd $DRINKAPPHOME || exit

# Install our dependencies
npm install

# Install pm2
npm install pm2

# Run our application in the background with pm2
#pm2 start $DRINKAPPHOME/index.js --name "drinkapp"

# Determine if our application is running. If running issue a restart. If not running (fresh install) then start it. If other then we have a problem...
PROC=$(pgrep -c -f index.js)

if [ "$PROC" -eq 0 ]; then
    # No instances running.
    pm2 start $DRINKAPPHOME/index.js
fi
if [ "$PROC" -eq 1 ]; then
    # Restart our instance
    pm2 restart $DRINKAPPHOME/index.js
fi
if [ "$PROC" -gt 1 ]; then
    # Something messed up and we have more than two instances - delete them and start a single instance.
    pm2 delete all
    pm2 start $DRINKAPPHOME/index.js
fi

# Exit
exit 0