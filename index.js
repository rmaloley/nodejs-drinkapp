// Require the Express framework
const express = require('express');

// Require body-parser
const bodyParser= require('body-parser')

// Define our application as an express() app
const app = express();

// Require the MongoDB files
const MongoClient = require('mongodb').MongoClient

// Define and get our server hostname.
var os = require("os");
var hostname = os.hostname();

// Define our db variable which will become our databse.
var db;

// Connect to our database. If we can't connect then do not start the app.
MongoClient.connect('mongodb://vm04/test', (err, database) => {
    // ... start the server
    if (err) return console.log(err);
    db = database;
    app.listen(80, function() {
        console.log('App listening on port 80!');
    });
})

// Tell our app to use the body-parser
app.use(bodyParser.urlencoded({extended: true}))

// Tell our app to set the view engine to ejs for templating our HTML.
app.set('view engine', 'ejs')

// We have access to the public folder.
app.use(express.static('public'))

// Display the main index.html file.
app.get('/', (req, res) => {

    // Render our web page.
    db.collection('drinks').find().toArray((err, result) => {
        if (err) return console.log(err)
        // renders index.ejs
        res.render('index.ejs', {drinks: result, host: hostname})
    })
});

// Someone submitted data. Lets do something! 
app.post('/submit', (req, res) => {
    
    // Save the form data to our database.
    db.collection('drinks').save(req.body, (err, results) => {
        if (err) return console.log(err);
        console.log(req.body);
        console.log('saved to database');
        res.redirect('/');
    })
});

// Lets go ahead and delete all of our data. Dangerous I know! 
app.post('/resetdb', (req, res) => {

    // DB command to drop everything from the database.
    db.collection('drinks').deleteMany({}, (err, results) => {
        if (err) return console.log(err);
         res.redirect('/');
    })
});

